archivemail (0.9.0-2) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Jonathan Dowland ]
  * Acknowledge NMU for #724043. Thanks, er, me!
    (Although the real fix is still forthcoming.)

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Use secure URI in debian/watch.
  * Bump debhelper from deprecated 7 to 12.
  * Add missing dependency on dh-python.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:11:21 -0500

archivemail (0.9.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Disable package build tests (temporarily) due to a broken
    test. Closes: #724043.

 -- Jonathan Dowland <jmtd@debian.org>  Wed, 20 Aug 2014 19:13:12 +0100

archivemail (0.9.0-1) unstable; urgency=low

  * New upstream release. Closes: #633408.
    - Drop patch for manpage installation path, which is now applied upstream.
    - Use xsltproc instead of docbook2man to build the manpage from source,
      which has been ported from SGML to XML. Adjust dependencies accordingly.
  * Move from python-support to dh_python2.
  * Cherry-pick patch fix-allows-to-in-manpage.diff from upstream, fixing a
    minor grammar error in the manpage.
  * Bump Standards-Version to 3.9.2 (no changes needed).

 -- Nikolaus Schulz <microschulz@web.de>  Sun, 10 Jul 2011 14:09:19 +0200

archivemail (0.8.2-1) unstable; urgency=low

  * New maintainer. Closes: #553592.
  * New upstream release; drop obsolete patches.
  * Bump up Standards-Version to 3.9.1 (no changes required)
  * Drop README.source, it's unnecessary for a source package in standard 3.0
    (quilt) format
  * Simplify debian/rules
  * Move fix of manpage installation path from debian/rules to a patch
    cherry-picked from upstream

 -- Nikolaus Schulz <microschulz@web.de>  Sat, 26 Mar 2011 17:45:15 +0100

archivemail (0.7.2-9) unstable; urgency=low

  * Increase minimum version of python-support to 0.90 for python2.6
  * Bump up Standards-Version to 3.9.0 (no changes)
  * debian/control:Maintainer: set to my debian email address

 -- Serafeim Zanikolas <sez@debian.org>  Sat, 24 Jul 2010 19:06:09 +0200

archivemail (0.7.2-8) unstable; urgency=low

  * Bump Standards-Version to 3.8.4 (no changes)
  * Switch dpkg-source format to 3.0 (quilt)
    * debian/control: drop Depends: quilt
    * debian/rules: drop patch-related stuff
  * Revise debian/rules to use dh overrides and depend on debhelper >= 7.0.50~
  * Drop unnecessary Build-Depends: python-dev

 -- Serafeim Zanikolas <serzan@hellug.gr>  Sat, 13 Feb 2010 19:57:14 +0100

archivemail (0.7.2-7) unstable; urgency=low

  * Relax the precision of comparisons of mailbox timestamps in test suite.
    Closes: #549736.
  * Bump up Standards-Version to 3.8.3
    - skip tests when nocheck is in DEB_BUILD_OPTIONS
    - add debian/README.source
    - add headers in quilt patches

 -- Serafeim Zanikolas <serzan@hellug.gr>  Sat, 10 Oct 2009 18:33:29 +0200

archivemail (0.7.2-6) unstable; urgency=low

  [ Serafeim Zanikolas ]
  * Do not overwrite <mailbox>_archive.gz when it is a symbolic link.
    Closes: #349068.
  * Add, document and test --prefix option for archive name. Closes: #247340.
  * Add, document and test --all option. Closes: #470675.
  * Switch patch management from dpatch to quilt.
  * Re-add ${misc:Depends} in control file.
  * Add Uploaders and Vcs-* fields as it is now maintained as part of the
    python-apps team.

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Serafeim Zanikolas <serzan@hellug.gr>  Sun, 22 Feb 2009 02:10:50 +0000

archivemail (0.7.2-5) unstable; urgency=low

  * New maintainer. Closes: #489448.
  * Fix watch file.
  * Move non-debian-specific patches from diff.gz to debian/patches.
  * Document --archive-name switch in manpage; add docbook and docbook-utils
    in build-deps.
  * Remove obsolete ${misc:Depends}.
  * Set Standards-Version to 3.8.0 (no changes required).

 -- Serafeim Zanikolas <serzan@hellug.gr>  Sun, 06 Jul 2008 18:36:12 +0100

archivemail (0.7.2-4) unstable; urgency=low

  * Orphaned the package.

 -- Joey Hess <joeyh@debian.org>  Sat, 05 Jul 2008 17:29:40 -0400

archivemail (0.7.2-3) unstable; urgency=low

  * debhelper v7, rules file minimisation
  * In python 2.5, mailbox objects evaluate to false when empty.
    Avoid an assertian when this happens. Closes: #476806

 -- Joey Hess <joeyh@debian.org>  Sat, 26 Apr 2008 01:17:31 -0400

archivemail (0.7.2-2) unstable; urgency=low

  * Improved description. Closes: #458196

 -- Joey Hess <joeyh@debian.org>  Mon, 10 Mar 2008 15:57:26 -0400

archivemail (0.7.2-1) unstable; urgency=low

  * New upstream release
    - Fixes IMAP message flag conversion, with a patch from Christian Brabandt
      Closes: #434807
    - Add --copy option. Closes: #434798
    - Expand tilde in argument of long option --pwfile. (Thanks Christian
      Brabandt) Closes: #434813
    - Supports autodetecting the IMAP folder separator. Closes: #368112
    - Several fixes already applied to the Debian package.
  * Dropped some old patches that I hope are no longer necessary.
  * Added usage help for --archive-name, a patch I've been carrying in
    the Debian package for too long unmerged.
  * Use debhelper v5.
  * Add Homepage field.

 -- Joey Hess <joeyh@debian.org>  Thu, 15 Nov 2007 19:18:02 -0500

archivemail (0.7.0-3) unstable; urgency=low

  * Apply a patch from Christian Brabandt to fix archiving mails from an imap
    server when using the --warn-duplicate switch. Closes: #434786

 -- Joey Hess <joeyh@debian.org>  Thu, 26 Jul 2007 15:45:03 -0400

archivemail (0.7.0-2) unstable; urgency=low

  * Apply patch for upstream bug #1670422, python 2.5 does not have
    message.fp.name and should instead use message.fp._file.name.

 -- Joey Hess <joeyh@debian.org>  Sun, 06 May 2007 16:30:16 -0400

archivemail (0.7.0-1) unstable; urgency=low

  * New upstream release, with many fixes, including these fixes to Debian
    bugs:
    - Fixed IMAP --delete which didn't work at all.  (Thanks Anand)
      Closes: #203282
    - Terminate each message in newly written mbox with an empty line if the
      message source is not an mbox-format folder.  (Thanks  Chung-chieh Shan)
      Closes: #250410
    - Mangle From_ in message body if the message source is not an mbox-format
      folder.  (Thanks Chung-chieh Shan)  Closes: #250402
    - Also merges most of my changes to the Debian package.
  * Re-enable test suite which works again.

 -- Joey Hess <joeyh@debian.org>  Fri, 10 Nov 2006 17:09:28 -0500

archivemail (0.6.2-5) unstable; urgency=low

  * Switch to using python-support, although there are no library files for it
    to deal with.

 -- Joey Hess <joeyh@debian.org>  Sun,  1 Oct 2006 12:56:28 -0400

archivemail (0.6.2-4) unstable; urgency=low

  * Man page update from Nikolaus Schulz to document --pwfile and
    --filter-append and SSL/IMAPS support. Closes: #386907
  * Patch from Nikolaus Schulz to fix the --pwfile and --filter-append
    option parsing. Closes: #386908
  * Patch from Nikolaus Schulz to fix issues in IMAP authentication
    introduced in 0.6.2. Closes: #386903

 -- Joey Hess <joeyh@debian.org>  Sun, 10 Sep 2006 21:23:09 -0400

archivemail (0.6.2-3) unstable; urgency=low

  * Fix a regression in CRLF conversion for IMAP messages introduced in 0.6.2.
    Thanks, Nikolaus Schulz. Closes: #386858

 -- Joey Hess <joeyh@debian.org>  Sun, 10 Sep 2006 14:27:56 -0400

archivemail (0.6.2-2) unstable; urgency=HIGH

  * Fix a number of temporary file security holes in archivemail and its
    test suite. CVE-2006-4245 Closes: #385253

 -- Joey Hess <joeyh@debian.org>  Wed, 30 Aug 2006 00:09:13 -0400

archivemail (0.6.2-1) unstable; urgency=low

  * New upstream release.
  * SSL support. Closes: #368117
  * Alternative fix for #297732.
  * Disabled the test suite, which was not updated to follow changes to
    --suffix behavior, and which also seems to have race conditions.
    For example, some code in it calulates a date N days ago, does some
    operations with that date, then constructs a file based on a date N
    days ago -- which can be a different day if run around midnight!

 -- Joey Hess <joeyh@debian.org>  Tue, 25 Jul 2006 23:38:18 -0400

archivemail (0.6.1-7) unstable; urgency=low

  * Patch from Nikolaus Schulz to fix code that is used (in some cases)
    to generate the From_ header for a mbox. Closes: #367017
  * Patch from Nikolaus Schulz to fix CRLF conversion from IMAP.
    Closes: #367033

 -- Joey Hess <joeyh@debian.org>  Wed, 24 May 2006 18:42:09 -0400

archivemail (0.6.1-6) unstable; urgency=low

  * Patch from Falko Trojahn to break up deletion of IMAP mail into multiple
    commands, to avoid problems with line length on some IMAP server.
    Closes: #297732

 -- Joey Hess <joeyh@debian.org>  Fri,  3 Feb 2006 15:10:31 -0500

archivemail (0.6.1-5) unstable; urgency=low

  * Switch watchfile to sf.net redirector.

 -- Joey Hess <joeyh@debian.org>  Sun, 17 Jul 2005 16:50:17 +0300

archivemail (0.6.1-4) unstable; urgency=low

  * Patch from Brian Thomas Sniffen to fix comparisons of rfc822.message
    objects, which fixes problems with maildirs appearing empty.
    Closes: #305902

 -- Joey Hess <joeyh@debian.org>  Thu, 26 May 2005 19:41:31 -0400

archivemail (0.6.1-3) unstable; urgency=low

  * Move deps from build-depends-indep to build-depends, to meet current
    policy.
  * Don't assign to None. Closes: #206397

 -- Joey Hess <joeyh@debian.org>  Tue, 30 Sep 2003 15:00:26 -0400

archivemail (0.6.1-2) unstable; urgency=low

  * Use dh_python.

 -- Joey Hess <joeyh@debian.org>  Thu, 21 Nov 2002 00:17:53 -0500

archivemail (0.6.1-1) unstable; urgency=low

  * New upstream release (no changes).

 -- Joey Hess <joeyh@debian.org>  Thu, 31 Oct 2002 13:46:28 -0500

archivemail (0.6.0-4) unstable; urgency=low

  * Turned off failing weird_headers test (patch from upstream).
    Closes: #167088

 -- Joey Hess <joeyh@debian.org>  Thu, 31 Oct 2002 13:26:00 -0500

archivemail (0.6.0-3) unstable; urgency=low

  * Don't qualify archivemail in archivemail_all. Closes: #165647

 -- Joey Hess <joeyh@debian.org>  Wed, 30 Oct 2002 11:09:05 -0500

archivemail (0.6.0-2) unstable; urgency=low

  * Updated package description to mention IMAP.

 -- Joey Hess <joeyh@debian.org>  Fri, 11 Oct 2002 15:55:50 -0400

archivemail (0.6.0-1) unstable; urgency=low

  * New upstream release with IMAP support.

 -- Joey Hess <joeyh@debian.org>  Sun,  6 Oct 2002 11:48:41 -0400

archivemail (0.5.1-1) unstable; urgency=low

  * New upstream release fixing tempfile bug. Closes: #152024, #162047

 -- Joey Hess <joeyh@debian.org>  Mon, 23 Sep 2002 20:01:09 -0400

archivemail (0.4.9-1) unstable; urgency=low

  * New upstream release, with more robust directory name support.
    Closes: #156267

 -- Joey Hess <joeyh@debian.org>  Sun, 18 Aug 2002 12:16:49 -0400

archivemail (0.4.8-1) unstable; urgency=low

  * New upstream release, with better temp file creation. Closes: #146594

 -- Joey Hess <joeyh@debian.org>  Tue, 21 May 2002 21:53:24 -0400

archivemail (0.4.7-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Thu,  9 May 2002 12:30:20 -0400

archivemail (0.4.5-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Wed,  1 May 2002 20:39:05 -0400

archivemail (0.4.3-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Thu, 25 Apr 2002 14:14:30 -0400

archivemail (0.4.0-1) unstable; urgency=low

  * First release.

 -- Joey Hess <joeyh@debian.org>  Wed, 17 Apr 2002 11:45:33 -0400
